package ch.blinkenlights.android.vanilla;

import android.app.Activity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class AdManager {

    // num click on cell before show ad
    public static int numClick = 0;
    public final static int numClickBeforeShowAd = 2;


    private static InterstitialAd interstitial;


    public static void loadFullScreenAd(Activity activity) {

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("E35D4295046810B35F7FE024C2B38D8C").build();
        interstitial = new InterstitialAd(activity);
        interstitial.setAdUnitId(activity
                .getString(R.string.interstitial));
        interstitial.loadAd(adRequest);

    }

    public static void showFullScreenAd(Activity context) {

        if (interstitial.isLoaded()) {
            interstitial.show();
            loadFullScreenAd(context);
        }

    }

    public static void showInterstitialWithLogic(Activity context) {

        AdManager.numClick++;
        if (AdManager.numClick >= AdManager.numClickBeforeShowAd) {
            AdManager.showFullScreenAd(context);
            AdManager.numClick = 0;
        }
    }


    public static void showInterstialFirst(final Activity context) {
        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                interstitial.show();
                loadFullScreenAd(context);
            }
        });
    }

}
